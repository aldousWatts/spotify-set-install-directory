:: fchooser.bat
:: launches a folder chooser and outputs choice to the console
:: https://stackoverflow.com/a/15885133/1683264

@echo off
setlocal

:: this is the prompt for the directory. sweet! ez. assigns to variable 'folder' which is called with !folder!
set "psCommand="(new-object -COM 'Shell.Application')^
.BrowseForFolder(0,'This script creates a Spotify folder with two subdirectories, Local and Roaming, that will contain Spotify installation files at the directory you choose here.',0,0).self.path""

for /f "usebackq delims=" %%I in (`powershell %psCommand%`) do set "folder=%%I"

:: apparently this allows !folder! to call the directory, or maybe resets from powershell to cmd. idk.
setlocal enabledelayedexpansion

:: kills script if the variable is empty because you hit cancel
IF [!folder!] == [] ( GOTO EndBatch )

echo You chose !folder! for the Spotify directory.
echo Creating the Spotify folders that will contain the app files in the above directory...
mkdir !folder!\Spotify
mkdir !folder!\Spotify\Roaming
mkdir !folder!\Spotify\Local
echo done.
echo Creating the symlinks that will point the installer to the chosen directory...
mklink /j "%appdata%\Spotify" "!folder!\Spotify\Roaming"
mklink /j "%localappdata%\Spotify" "!folder!\Spotify\Local"
echo done.
echo Spotify will now install to !folder!\Spotify. Take a moment to wonder why their developers chose not to include such a basic feature of a program as moving its install directory.

:: seems to end ability to call the !folder! variable
endlocal

pause

:EndBatch

:: spotifyMoveInstallDirectory.bat
:: launches a folder chooser and outputs choice to a script that changes the spotify install directory
:: aw2020, based on fchooser.bat on stackoverflow, whose link is in top comments

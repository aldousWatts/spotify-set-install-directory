## Spotify Windows App Installation Directory Changing Script/Prompt

It's the fine year 2020 and we still can't choose where the Spotify Windows Desktop App installs. This script makes it easy to choose where a fresh installation of Spotify will be by tricking the installer with symlinks.

## Running This Script

First, ensure Spotify is completely uninstalled and deleted from your computer. Check your %AppData% and %LocalAppData% and make sure there is not a Spotify folder in either. Delete them if they're there.

Either copy the spotifySetInstallDirectory.bat contents into a text file and name it whateveryouwant.bat and run it, or download it from here and run it. You might have to rename the downloaded file because it may be downloaded as spotifysid.bat.txt. Remove the .txt and leave it as spotifysid.bat, then run that file. The icon should look like a window with two gears in it.

When you open the file, you'll probably be presented with a "Windows protected your PC" warning box. Click "More info" and then "Run anyway".

Choose the directory you want the script to create the Spotify folder in.

Click OK. Done! Now, run the Spotify installer as normal.

***

The script is only a few lines, I encourage you to check it out before you run it. It will create a browse-for-file prompt, then take the choice from there and use it to make the new Spotify install directories and make the symlinks for you.

Hope this makes someone's Spotify experience a little easier. 

### Installing Spotify on External Media

While it's not a fully portable solution, you can choose to install Spotify on external media this way (like a USB drive) and it won't put much data on your computer at all.

### Other Users' AppData

If you install on a computer with other users, they will still get an %appdata%\Spotify and %localappdata%\Spotify folder in their user files, but they're nowhere near as huge as the main Spotify files. They're mostly user data and such, from what I saw.
